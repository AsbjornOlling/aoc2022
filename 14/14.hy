(import lib [readlines reclist first second flatten-once])
(require lib :readers [dbg])
(import numpy :as np)
(import hyrule [pp])
(import itertools [chain])
(require hyrule [ap-map ncut fn+ let+ loop ->> ->])

(defn parse-line [line]
  (list (map (fn [x] (tuple (map int (.split x ","))))
                     (.split (.strip line) " -> "))))

(defn read-input [fname]
  (list (map parse-line (readlines fname))))

; parsing done

; part 1

(defn max-xy [lines]
  ; find highest X and Y coordinates
  #((max (map first (chain #*lines)))
    (max (map second (chain #*lines)))))

(defn line-coords [line]
  ; all coords covered by a line with multiple segments
  (map
    (fn+ [[[x0 y0] [x1 y1]]]
      (list (cond
        (= x0 x1)
          (ap-map #(x0 it)
                  (range (min y0 y1)
                         (+ 1 (max y0 y1))))
        (= y0 y1)
          (ap-map #(it y0)
                  (range (min x0 x1)
                         (+ 1 (max x0 x1)))))))
    (zip (ncut line :-1) (ncut line 1:))))

(defn make-grid [lines]
  ; find all coords for all lines
  ; and dump 'em in a numpy array
  (let+ [[mx my] (max-xy lines)
         coords (set (flatten-once (flatten-once (map line-coords lines))))]
    (np.array (lfor y (range (+ my 1))
                    (lfor x (range (+ mx 1))
                          (in #(x y) coords))))))

(defn three-below [grid x y]
  ; three tiles below coord (x,y) in grid
  (if (<= (len grid) (+ y 1))
      []
      (->>
        (ncut grid
              (+ y 1)
              (: (- x 1) (+ x 2)))
        (map bool)
        (list))))

(defn place-sand-unit [grid [sourcex 500] [sourcey 0]]
  ; returns the coord where the sand unit will land
  (if (= True (get grid sourcey sourcex))
      None ;entrance blocked
      (loop [[x sourcex] [y sourcey]]
            (match (three-below grid x y)
                   [_ False _]
                   (recur x (+ y 1))

                   [False True _]
                   (recur (- x 1) (+ y 1))

                   [True True False]
                   (recur (+ x 1) (+ y 1))

                   [True True True]
                   #(x y)))))

(defn place-all-sand [grid]
  ; place sand in grid until it can no longer be placed
  (loop [[g grid] [count 0]]
    (if (setx sandcoord (place-sand-unit g))
        (do
          (setv (get g (second sandcoord) (first sandcoord)) True)
          (recur g (+ 1 count)))
        count)))

(print "⭐ Day 14 part 1:"
       (place-all-sand (make-grid (read-input "input.txt"))))

; part 2

(defn extend-grid [grid]
  ; extend east and downwards, we already have plenty of space to the right
  (-> grid
    ; add area right
    (np.pad #(#(0 0) #(0 500)) :constant-values False)
    ; add empty row below
    (np.pad #(#(0 1) #(0 0)) :constant-values False)
    ; add floor blow that
    (np.pad #(#(0 1) #(0 0)) :constant-values True)))

;(print "⭐ Day 14 part 1:" (place-all-sand (extend-grid (make-grid (read-input "input.txt")))))


; drawing time

(import pygame)

(defn pixel-at [x y]
  #dbg #(x y)
  (pygame.draw.rect (pygame.display.get-surface)
                    #(0xff 0 0)
                    (pygame.Rect x y 1 1)))

(pygame.init)
(let [grid (extend-grid (make-grid (read-input "input.txt")))
      coords (zip #*(reversed (np.where grid)))]
  (pygame.display.set_mode #((len (get grid 0)) (len grid)))
  (for [c coords]
    (pixel-at #*c)))

(pixel-at 0 0)
(pixel-at 10 10)
(pixel-at 20 20)
(pixel-at 30 30)
(pygame.display.update)

(import time)
(time.sleep 5)
