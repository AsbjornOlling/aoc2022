(require lib :readers [dbg time])
(import lib [first rest])
(import itertools [chain])
(import functools [reduce])
(require hyrule [ap-map fn+ let+ ->>])
(import time [sleep])
(import numpy :as np)
(import sys)
(sys.setrecursionlimit 200_000)

; 🔨🔨🔨

(defmacro Monkey [monkeynum
                  _starting _items items
                  _operation _new _eq oparg1 opop oparg2
                  _test _divisible _by modby
                  _if _true _throw _to _tomonkey truetomonkey
                  __if __false __throw __to __monkey falsetomonkey]
  (let [divisible? `(fn [a] (not (% a ~modby)))]
    `{"id" ~monkeynum
      "items" ~items
      "op" (fn [old] (% (~opop ~oparg1 ~oparg2)
                        ~(* 2 3 5 7 11 13 17 19)))
      "throwto" (fn [wlvl] (if (~divisible? wlvl)
                               ~truetomonkey
                               ~falsetomonkey))}))


(defn inspect-item [monkey]
  (let [item     (first (get monkey "items"))
        ;wlvl     (// ((get monkey "op") item) 3)
        wlvl     ((get monkey "op") item)
        tomonkey ((get monkey "throwto") wlvl)]
    {"monkey" (| monkey {"items" (rest (get monkey "items"))})
     "throws" {tomonkey [wlvl]}}))


(defn join-dicts [a b]
  (dfor
    k (chain (.keys a) (.keys b))
    k (+ (.get a k [])
         (.get b k []))))


(defn inspect-items [monkey]
  (if (not (get monkey "items"))
      ; base case - no items
      {"monkey" monkey "throws" {}}
      ; inspect an item and recurse
      (let [this (inspect-item monkey)
            rec (inspect-items (get this "monkey"))]
        {"monkey" (get rec "monkey")
         "throws" (join-dicts (get this "throws")
                              (get rec "throws"))})))

(defn apply-throws [throws monkeys]
  (lfor
    m monkeys
    (| m {"items" (+ (get m "items")
                     (.get throws (get m "id") []))})))

(defn find-monkey [mid monkeys]
  (next (gfor m monkeys
              :if (= mid (get m "id"))
              m)))

(defn turn [turnid monkeys]
  (let [turnmonkey (find-monkey turnid monkeys)
        result     (inspect-items turnmonkey)]
    (apply-throws
      (get result "throws")
      (lfor m monkeys ; replace turn monkey with result monkey
          (if (= turnid (get m "id"))
              (get result "monkey")
              m)))))

(defn round-and-count [monkeys]
  (reduce (fn+ [[insps ms] turnid]
            #((+ insps
                 [(len (get (find-monkey turnid ms) "items"))])
              (turn turnid ms)))
          (ap-map (get it "id") monkeys)
          #([] monkeys)))

(defn do-n-rounds-and-count [monkeys [n 10_000] [acc #([] [])]]
  (if (= n 0)
      acc
      (let+ [[newcounts newmonkeys] (round-and-count monkeys)]
        (do-n-rounds-and-count newmonkeys
                               (- n 1)
                               #([#*(get acc 0) newcounts]
                                 [#*(get acc 1) newmonkeys])))))

; look mom i did a macro
(setv monkeys [
  (Monkey 0
    Starting items: [59 74 65 86]
    Operation: new = old * 19
    Test: divisible by 7
      If true: throw to monkey 6
      If false: throw to monkey 2)

  (Monkey 1
    Starting items: [62 84 72 91 68 78 51]
    Operation: new = old + 1
    Test: divisible by 2
      If true: throw to monkey 2
      If false: throw to monkey 0)

  (Monkey 2
    Starting items: [78 84 96]
    Operation: new = old + 8
    Test: divisible by 19
      If true: throw to monkey 6
      If false: throw to monkey 5)

  (Monkey 3
    Starting items: [97 86]
    Operation: new = old * old
    Test: divisible by 3
      If true: throw to monkey 1
      If false: throw to monkey 0)

  (Monkey 4
    Starting items: [50]
    Operation: new = old + 6
    Test: divisible by 13
      If true: throw to monkey 3
      If false: throw to monkey 1)

  (Monkey 5
    Starting items: [73 65 69 65 51]
    Operation: new = old * 17
    Test: divisible by 11
      If true: throw to monkey 4
      If false: throw to monkey 7)

  (Monkey 6
    Starting items: [69 82 97 93 82 84 58 63]
    Operation: new = old + 5
    Test: divisible by 5
      If true: throw to monkey 5
      If false: throw to monkey 7)

  (Monkey 7
    Starting items: [81 78 82 76 79 80]
    Operation: new = old + 3
    Test: divisible by 17
      If true: throw to monkey 3
      If false: throw to monkey 4)])

(setv _monkeys
      [(Monkey 0
         Starting items: [79 98]
         Operation: new = old * 19
         Test: divisible by 23
         If true: throw to monkey 2
         If false: throw to monkey 3)

       (Monkey 1
         Starting items: [54 65 75 74]
         Operation: new = old + 6
         Test: divisible by 19
         If true: throw to monkey 2
         If false: throw to monkey 0)

       (Monkey 2
         Starting items: [79 60 97]
         Operation: new = old * old
         Test: divisible by 13
         If true: throw to monkey 1
         If false: throw to monkey 3)

       (Monkey 3
         Starting items: [74]
         Operation: new = old + 3
         Test: divisible by 17
         If true: throw to monkey 0
         If false: throw to monkey 1)])

(assert
  (= 10197
    (* #*(cut (list (reversed (sorted (map sum (zip #*(first (do-n-rounds-and-count _monkeys 20))))))) 2))))
(print "sanity check passed")


#time(do-n-rounds-and-count monkeys 20)
#time(do-n-rounds-and-count monkeys 100)
#time(do-n-rounds-and-count monkeys 200)
#time(do-n-rounds-and-count monkeys 300)
#time(do-n-rounds-and-count monkeys 400)
#time(do-n-rounds-and-count monkeys 500)
#time(do-n-rounds-and-count monkeys 600)
#time(do-n-rounds-and-count monkeys 700)
#time(do-n-rounds-and-count monkeys 800)

(print "Starting 10k copmutation")
(print
  "⭐Day 11 part 2:"
  (->>
    #time(do-n-rounds-and-count monkeys)
    ; sorry
    (first)
    (unpack-iterable)
    (zip)
    (map sum)
    (sorted)
    (reversed)
    (list)
    ((fn [x] (cut x 2)))
    (unpack-iterable)
    (*)))
