(setv calorie-sums
  ; split on empty lines and sum the ints between
  (list (map
    (fn [s] (sum (map int (.split s "\n"))))
    (.split (.strip (.read (open "input"))) "\n\n"))))

(print
  "⭐ day 01 part 1:"
  (max calorie-sums))

(print
  "⭐ day 01 part 2:"
  (sum (cut (sorted calorie-sums
                    :reverse True)
            3)))
