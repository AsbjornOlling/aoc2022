(require hyrule [->> defn+])

(defn+ to-range [[start stop]]
  (range start (+ 1 stop)))

(defn parse-range [rangestr]
  ; parses string like "5,15" into object like `range(5,16)`
  (->> (.split rangestr "-")
       (map int)
       (tuple)
       (to-range)))

(defn parse-line [line]
  ; parse one line into a tuple of `range` objects
  (->> (.split (.strip line) ",")
       (map parse-range)
       (tuple)))

(defn range-pairs [fname]
  ; read input file, return list[tuple[range, range]]
  (->> (open fname)
       (.readlines)
       (map parse-line)
       (list)))

; ok that's enough parsing. let's do part 1

(defn count [f iter]
  (len (list (filter f iter))))

(defn contained? [outer inner]
  (and (<= outer.start
           inner.start)
       (<= inner.stop
           outer.stop)))

(defn+ either-contained? [[a b]]
  (or (contained? a b)
      (contained? b a)))

; that was simple enough - count 'em

(print
  "⭐ Day 04 part 1:"
  (count either-contained? (range-pairs "input.txt")))


; part 2 - just use a new predicate func

(defn+ overlaps? [[a b]]
  (& (set a) (set b)))

(print
  "⭐ Day 04 part 2:"
  (count overlaps? (range-pairs "input.txt")))
