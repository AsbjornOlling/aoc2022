{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = [
    pkgs.hy
    pkgs.python310Packages.hyrule
    pkgs.python310Packages.numpy
    pkgs.python310Packages.networkx
    pkgs.python310Packages.scipy
  ];
  shellHook = ''
    PYTHONDONTWRITEBYTECODE=1
  '';
}
