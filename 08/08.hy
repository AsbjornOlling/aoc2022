(require hyrule [-> ->> ncut])
(import hyrule [pp])
(import lib [first second rest])
(import numpy :as np)
(import functools [reduce])
(import itertools [takewhile])
(import time)
(setv input-grid (np.genfromtxt "input.txt"
                                :delimiter 1
                                :dtype np.int8))
(setv input-padded (np.pad input-grid 1))
; wow parsing was easy today
; on to the real thing: part 1

(defn acc-max [row]
  ; make a row of acummulating `max`
  (reduce
    (fn [acc it] (+ acc [(max (max acc) it)]))
    (rest row)
    [(first row)]))

(defn max-from-left [grid]
  ; make grid of the left visibility-threshold
  (->> grid
       (map acc-max)
       (list)
       (np.array)))

(defn prepend-col [grid]
  ; prepend a column of -1 to np array
  (np.append (* [[-1]] (second grid.shape)) grid :axis 1))

(defn append-col [grid]
  ; append a column of -1 to np array
  (np.append grid (* [[-1]] (second grid.shape)) :axis 1))

(defn visible-from-left [grid]
  ; return an np array of bools
  ; denoting whether the tile is visible from the left
  (ncut (< (prepend-col (max-from-left grid))
           (append-col grid))
        : :-1))

(defn apply-rotated [f rot it]
  ; rotate a numpy array, apply f, then rotate back
  (-> it
      (np.rot90 rot)
      (f)
      (np.rot90 (* -1 rot))))

(defn apply-all-rotations [f it]
  ; apply a function across all 4 cardinal diractions
  (let [left  (f it)
        up    (apply-rotated f 1 it)
        down  (apply-rotated f -1 it)
        right (apply-rotated f 2 it)]
    [left up down right]))

(defn visible-from-cardinals [grid]
  ; apply element-wise or along all four directions
  ; to return a bool grid denoting visibility
  (reduce np.logical_or (apply-all-rotations visible-from-left grid)))

(setv before (time.time))
(print
  "⭐ Day 08 Part 1:"
  (np.sum (visible-from-cardinals input-grid)))
(print (- (time.time) before))

; okay that was part 1
; lets do part 2

(defn distance-left [backview n]
  ; given all the tiles to the left of this tile `n`
  ; return how many trees are visible
  (let [backwards (list (reversed backview))
        viewtiles (list (takewhile (fn [x] (< x n))
                                   backwards))]
    (if (= (len viewtiles) (len backview))
        ; case: can see out of forest from here
        (len viewtiles)
        ; case: view obstructed by tree
        ; remember to count the obstructing tree
        (+ 1 (len viewtiles)))))

(defn step [acc it]
  ; reduce function for `left-distance-row`
  ; returns the view distance to the left for a tile with num `it`
  ; given the tiles to the left as `backview`
  {"viewmap"  (+ (get acc "viewmap")
                 [(distance-left (get acc "backview") it)])
   "backview" (+ (get acc "backview")
                 [it])})

(defn left-distance-row [row]
  ; calculate view distance to the left of each item in a row
  (->
    (reduce step row {"viewmap" [] "backview" []})
    (get "viewmap")))

(defn left-distance-map-from-grid [grid]
  ; return a grid of distances to the left
  (->> grid
       (map left-distance-row)
       (list)
       (np.array)))

(defn distance-map-from-grid [input-grid]
  ; find grids of distances in all 4 directions
  ; and multiply them element-wise, returning a grid of distance scores
  (* #*(apply-all-rotations left-distance-map-from-grid input-grid)))

; phew that was quite a lot
; lets get the answer

(setv before (time.time))
(print
  "⭐ Day 08 Part 2:"
  (np.max (distance-map-from-grid input-grid)))
(print (- (time.time) before))
