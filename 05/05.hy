(require hyrule [ap-filter ncut ->>])
(import hy.pyops [reduce])
(import hyrule [butlast])
(import string [ascii_letters])

(defn filter-letters [strtuple]
  (list (ap-filter (in it ascii_letters)
                   strtuple)))

(defn read-stacks [fname]
  (->>
    ; read first section of file
    (.read (open fname))
    ((fn [x] (get (.split x "\n\n") 0)))
    ((fn [x] (.split x "\n")))
    (butlast)
    ; rotate from rows into columns
    ((fn [x] (zip #*x)))
    ; keep only columns with letters
    (map filter-letters)
    (filter bool) ; remove empty lists
    (list)))

(defn read-instructions [fname]
  (->>
    ; read last section of file
    (.read (open fname))
    ((fn [x] (get (.split x "\n\n") 1)))
    ; split into lines
    ((fn [x] (.split x "\n")))
    (butlast)
    ; split each line on spaces
    (map str.split)
    ; keep every other element, cast them to int
    (map (fn [x] (map int (ncut x 1::2))))))

; that's parsing done. on to the real task

; part 1
(defn move [amount from to [order reversed]]
  ; output a function that executes the said move
  (fn [stacks]
    (lfor x (enumerate stacks 1)
          (match x
            ; `from` stack: remove `amount` leading items
            #(i stack) :if (= i from)
              (ncut stack (: amount (len stack)))

            ; `to` stack: prepend `amount` items from `from` stack
            ; prepended items are fed through `order` function
            #(i stack) :if (= i to)
              (let [fromstack (get stacks (- from 1))
                    prepend   (ncut fromstack (: 0 amount))]
                [#*(order prepend) #*stack])

            ; leave all other stacks alone
            #(_ stack) stack))))

(defn top-of-stacks [stacks]
  ; string comprised of top elements of each stack
  (str.join "" (map (fn [x] (get x 0))
                    stacks)))

(print
  "⭐ Day 05 Part 1:"
  (top-of-stacks
    ; fold over list of instructions, applying each instr to stacks
    (reduce
      (fn [stacks instr]
        ((move #*instr) stacks))
      (read-instructions "input.txt")
      (read-stacks       "input.txt"))))

; part two

(print
  "⭐ Day 05 Part 2:"
  (top-of-stacks
    ; this time the `order` function does nothing
    (reduce
      (fn [stacks instr]
        ((move #*instr :order (fn [x] x)) stacks))
      (read-instructions "input.txt")
      (read-stacks       "input.txt"))))
