(require hyrule [->>])
(setv raw_backpacks (list (map (fn [x] (.strip x)) (.readlines (open "input")))))
; phew- ugly parse

(defn split-backpack [bp]
  ; split backpack into right and left halves
  (let [l (len bp)]
    #((cut bp (// l 2))
      (cut bp (// l 2) l))))


(defn find-double-item [bp]
  ; find item that's in both halves
  (.pop (& (unpack-iterable (map set bp)))))


(defn priority [item]
  ; find item priority by offset from ascii value
  (cond
    (>= item "a") (+ (- (ord item) (ord "a")) 1)
    (<= item "Z") (+ (- (ord item) (ord "A")) 27)))


(print
  "⭐ Day 03 part 01:"
  (sum (->>
    raw_backpacks
    (map split-backpack)
    (map find-double-item)
    (map priority))))


; begin part two


(defn groups [bps]
  ; split into groups of size 3
  (lfor
    i (range (// (len bps) 3))
    [(set (get bps (* i 3)))
     (set (get bps (+ 1 (* i 3))))
     (set (get bps (+ 2 (* i 3))))]))


(defn common-among-group [group]
  ; find item present in all backpacks using set intersections
  (.pop (& (unpack-iterable group))))


(print
  "⭐ Day 03 part 02:"
  (->>
    (groups raw_backpacks)
    (map common-among-group)
    (map priority)
    (sum)))
