(import networkx :as nx)
(import numpy :as np)
(import functools [reduce])
(require lib :readers [dbg])
(require hyrule [ncut ->> let+ defn+ fn+ ap-filter ap-map])

(setv str-input (np.genfromtxt "input.txt"
                               :delimiter 1
                               :dtype str))

(defn coord-of [arr val]
  (let [res (np.where (= arr val))]
    #((get res 1 0)
      (get res 0 0))))

(setv startcoord (coord-of str-input "S"))
(setv endcoord (coord-of str-input "E"))

(defn replace-start-and-end [arr]
  ((np.vectorize (fn [c] (cond
                           (= c "E") "z"
                           (= c "S") "a"
                           True      c)))
   arr))

(setv input ((np.vectorize ord) (replace-start-and-end str-input)))

(defn move-directions [heightmap]
  ; returns array with shape [4,y,x]
  ; indicating whether you can move loeft, down, up, or right from a given tile
  (let [padded (np.pad heightmap 1 :constant-values 200)
        height-left  (np.roll padded 1 :axis 1)
        height-down  (np.roll padded -1 :axis 0)
        height-up    (np.roll padded 1 :axis 0)
        height-right (np.roll padded -1 :axis 1)]
    (ncut (np.array [(<= (- height-left padded) 1)
                     (<= (- height-down padded) 1)
                     (<= (- height-up padded) 1)
                     (<= (- height-right padded) 1)])
           : 1:-1 1:-1)))


(defn+ edges [dirmap [x y]]
  ; returns a tuple of 2d coords representing a graph edge
  (reduce (fn+ [acc [canmove [xoff yoff]]]
            (if canmove
                (+ acc [#(#(x y)
                          #((+ x xoff) (+ y yoff)))])
                acc))
          (zip (ncut dirmap : y x)
               [#(-1 0) #(0 1) #(0 -1) #(1 0)])
          []))

(defn all-coords [heightmap]
  ; all coords in the heightmap
  (lfor x (range (len (get heightmap 0)))
        y (range (len heightmap))
        #(x y)))

(defn flatten-once [lst]
  (lfor y lst
        x y
        x))

(defn+ in-grid? [[x y]]
  (and
    (in y (range (get input.shape 0)))
    (in x (range (get input.shape 1)))))

(defn filter-edges [edges]
  (filter
    (fn+ [[p1 p2]] (and (in-grid? p1) (in-grid? p2)))
    edges))

(defn make-graph [heightmap]
  (let [md (move-directions heightmap)]
    (->>
      (all-coords heightmap)
      (ap-map (edges md it))
      (flatten-once)
      (filter-edges)
      (list)
      (nx.DiGraph))))

(setv g (make-graph input))
(print
  "⭐ Day 12 part 1"
  (- (len (nx.shortest-path g startcoord endcoord)) 1))

(print
  "⭐ Day 12 part 2"
  (->> ; lol the only as with access to bs are in the leftmost column
    (range (len input))
    (ap-map (nx.shortest-path g #(0 it) endcoord))
    (map len)
    (min)))
