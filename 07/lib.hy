; I'm getting annoyed at some of the decisions in hy's standard library,
; so I'm gonna start collecting a small utility library

(defn first [lst]
  (get lst 0))

(defn second [lst]
  (get lst 1))

(defn rest [lst]
  (cut lst 1 (len lst)))

(defn readlines [fname]
  (tuple (map str.strip (.readlines (open fname)))))

(defn id [x] x)
