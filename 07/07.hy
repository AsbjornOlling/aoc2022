(require hyrule [->> ap-filter ap-map let+])
(import lib [first second rest readlines id])
(import hyrule [pprint])
(import itertools [chain])

(defn build-tree [lines [dirname ""] [acc []]]
  (if (not lines)
      #(acc []) ; base case, nothing to do
      ; otherwise: parse command
      (match (.split (first lines))
             ["$" "cd" ".."]
               ; done with dir - return files and unprocessed commands
               #(acc (rest lines))

             ["$" "cd" dirname]
               ; build a new directory!
               ; recursively get child items (+ unparsed commands)
               (let+ [[items restlines] (build-tree (rest lines) dirname)]
                 (->>
                   ; make the new directory
                   (+ acc [{"dir" dirname "items" items}])
                   (build-tree restlines dirname))) ; iterate

             ["$" "ls"]
               ; do nothing - iterate
               (build-tree (rest lines) dirname acc)

             ["dir" _]
               ; do nothing - iterate
               (build-tree (rest lines) dirname acc)
               ; (we record new dirs on `cd`)

             [size filename]
               ; iterate
               (build-tree (rest lines)
                           dirname
                           ; add new file
                           (+ acc [{"file" filename
                                    "size" (int size)}])))))

(defn tree-from-file [fname]
  ; returns a filesystem tree from file `fname`
  ; this is the function to be used
  (first (first (build-tree (readlines fname)))))

(pprint (tree-from-file "testinput.txt"))

; okay that's parsing. now we have a filesystem tree.
; let's do part one
; find directories with size under 100_000

(defn directory-size [dir]
  (->>
    (get dir "items")
    (ap-map (if (in "dir" it)
                (directory-size it)
                (get it "size")))
    (sum)))


(defn all-dirs [dir]
  (let [subdirs (->> (get dir "items")
                     (ap-filter (in "dir" it))
                     (list))]
    (chain subdirs #*(map all-dirs subdirs))))


(print
  "⭐ Day 07 part 1:"
  (->>
    (tree-from-file "input.txt")
    (all-dirs)
    (map directory-size)
    (ap-filter (< it 100_000))
    (sum)))

; part 2
; beh

(defn need-to-delete [tree [target-usage 40_000_000]]
  (- (directory-size tree) target-usage))

(print
  "⭐ Day 07 part 2:"
  (let [tree (tree-from-file "input.txt")]
    (->>
      (all-dirs tree)
      (map directory-size)
      (ap-filter (<= (need-to-delete tree) it))
      (min))))
