(require hyrule [ncut loop])
(import hyrule [inc])

(defn distinct? [lst]
  (= (len lst)
     (len (set lst))))

(defn find-packet-header [stream [n 4]]
  (loop [[s stream] [i 0]]
    (if (distinct? (cut s n))
        (+ n i) ; <- the answer
        (recur (ncut s 1:) (inc i)))))

(setv raw-text (.read (open "../06/input.txt" "r")))

(print "⭐ Day 06 part 1:" (find-packet-header raw-text))
(print "⭐ Day 06 part 2:" (find-packet-header raw-text 14))
