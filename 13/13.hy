(require lib :readers [dbg time])
(require hyrule [fn+])
(import lib [flatten-once])
(import functools)

(setv input
      (list (map (fn [s] (list (map eval (.split (.strip s) "\n"))))
                 (.split (.strip (.read (open "input.txt"))) "\n\n"))))

; part 1 yay

(defn compare [a b]
  (match [a b]
         [[] [b #*bs]] ; left empty first
           True

         [[a #*as] []] ; right empty first
           False

         [[] []] ; list same lengths and values
           None

         [[a #*as] [b #*bs]] ; two non-empty lists
           (let [res (compare a b)]
             (if (= res None) (compare as bs) res))

         [i [#*xs]] ; mixed types: integer and list
           (compare [i] xs)

         [[#*xs] i] ; mixed types: list and integer
           (compare xs [i])

         [i j] ; both are integers
           (if (= i j) None (< i j))))

(print
  "⭐ Day 13 part 1:"
  (sum (map
    (fn+ [[i x]] (if (compare #*x) (+ 1 i) 0))
    (enumerate input))))

; begin part two

(setv part2input (+ [[[2]] [[6]]] (flatten-once input)))

; tiny wrapper function that turns my `compare`
; into a valid `key` function for python's `sorted`
(defn [functools.cmp-to-key] cmp [a b]
  (match (compare a b)
         True -1
         False 1
         None 0))

(print
  "⭐ Day 13 part 2:"
  (let [ordered (sorted part2input :key cmp)]
    (* (+ 1 (.index ordered [[2]]))
       (+ 1 (.index ordered [[6]])))))
