(require hyrule [ap-map ap-reduce fn+ -> ->> defn+])
(require lib :readers [dbg])
(import lib [readlines last rest first])
(import itertools [cycle takewhile accumulate])

(defn parse-line [line]
  (match (.split (.strip line))
         ["addx" num] (int num)
         ["noop"]     0))

(setv input (list (map parse-line (readlines "input.txt"))))

; part 1

(defn expand-addx [instrs]
  ; prepend a `noop` before each `addx`
  (ap-reduce (+ acc (if (!= it 0)
                        [0 it]
                        [0]))
             instrs []))

(defn x-values [instrs]
  ; find values of `x` at each clock cycle
  (->> (expand-addx instrs)
       (accumulate :initial 1)
       (list)))

(defn get-score [xs i]
  (* (get xs (- i 1)) i))

(defn part1 [input]
  (let [xs (x-values input)]
      (sum (ap-map (get-score xs it)
                   (range 20 221 40)))))

(print "⭐ Day 10 part 1: " (part1 input))

; part 2

(defn+ sprite-visible? [[scan sprite]]
  (<= (abs (- scan sprite)) 1))

(defn scan [xs]
  (map sprite-visible?
       (zip xs (cycle (range 40)))))

(defn to-char [b]
  (if b "⭐" "  "))

(defn display [scans]
  ; make printable display string from list[bool]
  (->>
    scans
    ; enumerate modulo 40
    (zip (cycle (range 40)))
    ; replace bools with chars, insert newlines
    (ap-map (match it
                   #(0 b) f"\n{(to-char b)}"
                   #(_ b) (to-char b)))
    (str.join "")
    (rest)))

(defn part2 [input]
  (->>
    (x-values input)
    (scan)
    (display)))

(print "⭐ Day 10 part 2:")
(print (part2 input))

; let's see if we can make a macro
; i want to make the input code valid hy syntax

(defn solve-string [part raw-string]
  (let [lines (.split (.strip raw-string) "\n")
        parsed (ap-map (parse-line (.strip it))
                       lines)]
    (match part
           1 (part1 parsed)
           2 (part2 parsed))))

(defreader elfcode
  ; parse part num
  (.slurp-space &reader)
  (setv partnum (int (.getc &reader)))
  (assert (in partnum [1 2]))
  ; read rest
  (defn read-until-pound []
    (if (= "#" (.peekc &reader))
        (* 0 (.getc &reader))
        (+ (.getc &reader)
           (read-until-pound))))
  `(solve-string ~partnum ~(.strip (read-until-pound))))


; small tests
; really just to show off the macro 🏄

(assert (= "⭐⭐⭐⭐ "
           (cut #elfcode 2
                  noop
                  addx 1
                  addx -2
                  addx -3
                  noop
                  noop
                  noop
                  addx 5#
                5)))

#elfcode 2
  noop
  addx 2
  addx -5
#
