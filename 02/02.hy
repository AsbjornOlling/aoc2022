(require hyrule [->>])

(setv raw_input (.read (open "input")))

(defn parse-symbol-a [x]
  (match x
    (| "A" "X") "rock"
    (| "B" "Y") "paper"
    (| "C" "Z") "scissors"))

(defn parse-input [raw symbolf]
  (->>
    ; split text into lines
    (.split (.strip raw) "\n")
    ; split each line into symbols
    (map (fn [line] (.split line " ")))
    ; apply f on all symbols
    (map (fn [x] (map symbolf x)))))

; that's enough input parsing... lets get to the stuff

(defn result-points [a x]
  (match [a x]
    ["rock"     "paper"]    6
    ["paper"    "scissors"] 6
    ["scissors" "rock"]     6
    [_ _] :if (= a x)       3
    [_ _]                   0))

(defn symbol-points [x]
  (match x
    "rock"     1
    "paper"    2
    "scissors" 3))

(defn round-points [a x]
  (+
    (result-points a x)
    (symbol-points x)))

(print
  "⭐ Day 02 part 01:"
  (sum (map (fn [x] (round-points (unpack-iterable x)))
            (parse-input raw_input parse-symbol-a))))

; okay that was part 1-
; lets go part 2

(defn parse-symbol-b [x]
  (match x
    "A" "rock"
    "B" "paper"
    "C" "scissors"
    "X" "lose"
    "Y" "tie"
    "Z" "win"))

(defn select-response [a x]
  (match [a x]
    ["rock"     "win"]  "paper"
    ["rock"     "lose"] "scissors"
    ["paper"    "win"]  "scissors"
    ["paper"    "lose"] "rock"
    ["scissors" "win"]  "rock"
    ["scissors" "lose"] "paper"
    [_          "tie"]  a))

(defn strategy-points [a x]
  (let [response (select-response a x)]
    (round-points a response)))

(print
  "⭐ Day 02 part 02:"
  (sum (map (fn [x] (strategy-points (unpack-iterable x)))
            (parse-input raw_input parse-symbol-b))))
