; I'm getting annoyed at some of the decisions in hy's standard library,
; so I'm gonna start collecting a small utility library

(defn first [lst]
  (get lst 0))

(defn second [lst]
  (get lst 1))

(defn rest [lst]
  (cut lst 1 (len lst)))

(defn last [lst]
  (get lst -1))

(defn readlines [fname]
  (tuple (map str.strip (.readlines (open fname)))))

(defn id [x] x)

(defreader dbg
  (setv parsed None)
  (setv str (.getc &reader))
  ; read one char at a time until it parses
  (while (not parsed)
    (try
      (setv parsed (hy.read str))
      (except [Exception]
        (setv str (+ str (.getc &reader))))))
  ; return a quoted expression
  `(let [result ~parsed]
    (print f"{~str} = {result}")
    result))
