(require hyrule [let+ defn+ fn+ branch])
(import hyrule [pp])
(import lib [readlines first second rest last])
(import functools [reduce])

(defn parse-line [x]
  (let+
    [[direction distance] (.split (.strip x) " ")]
    #(direction (int distance))))

(setv input (list (map parse-line (readlines "input.txt"))))
(setv input (list (map parse-line (readlines "input.txt"))))

; okay that's parsing, let's do stuff

(defn submax [a b]
  ; subtract b from a, bounded in [-1;1]
  (let [val (- a b)]
    (branch it
            (< val -1) -1
            (< 1 val)  1
            else       val)))

(defn+ touching? [[xt yt] [xh yh]]
  (and (<= (abs (- xt xh)) 1)
       (<= (abs (- yt yh)) 1)))

(defn+ move-knot [[xh yh] [xt yt]]
  ; move tail towards head
  (if (touching? #(xt yt) #(xh yh))
      #(xt yt)
      #((+ xt (submax xh xt))
        (+ yt (submax yh yt)))))

(defn+ move-head [[xh yh] direction]
  (match direction
         "R" #((+ xh 1) yh)
         "L" #((- xh 1) yh)
         "U" #(xh (- yh 1))
         "D" #(xh (+ yh 1))))

(defn move-knots [head knots]
  ; move a list of knots
  ; each according to the new position of the knot ahead
  (if (= knots [])
      []
      (let [moved-knot (move-knot head (first knots))]
        (+ [moved-knot]
           (move-knots moved-knot (rest knots))))))

(defn prepend-column [col paths]
  (assert (= (len col) (len paths)))
  (list (map (fn+ [[it path]] (+ [it] path))
             (zip col paths))))

(defn+ execute-move [[direction distance] head knots]
  ; execute a move, returns list of head and tail positions
  (if (= distance 0)
      ; base case
      (list (+ [[head]] (list (map (fn [x] [x]) knots))))
      ; move all knots 1
      (let [new-head (move-head head direction)
            new-knots (move-knots new-head knots)
            ; do the rest of the moves
            recur-result (execute-move [direction (- distance 1)]
                                       new-head
                                       new-knots)]
        ; concatenate results
        (prepend-column (+ [new-head] new-knots)
                        (list recur-result)))))

(defn execute-moves [moves [num-knots 2]]
  (reduce (fn [paths move]
            (let [new-paths (execute-move move
                                          (last (first paths))
                                          (list (map last (rest paths))))]
              (list (map (fn+ [a b] (+ a b))
                         paths new-paths))))
          moves
          (* num-knots [[#(0 0)]])))


(print
  "⭐ Day 09 part 1:"
  (len (set (second (execute-moves input)))))

(print
  "⭐ Day 09 part 2:"
  (len (set (last (execute-moves input 10)))))
