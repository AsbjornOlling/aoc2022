(import lib [second first])
(require lib :readers [dbg])
(require hyrule [-> defn+ fn+ ap-map ap-filter])
(import functools [lru-cache cache])
(import itertools [product chain])
(import tqdm [tqdm])

(defmacro Sensor [_at sx sy _closest _beaking _is __at bx by]
  (defn parse-num [sym]
    (-> (str sym)
        (.removesuffix ",")
        (.removesuffix ":")
        (.split "=")
        (get 1)
        (int)))
  `#(#(~(parse-num sx) ~(parse-num sy))
     #(~(parse-num bx) ~(parse-num by))))


; that's parsing
; let's do part 1

(defn [cache] manhattan [a b]
  (+ (abs (- (get a 0) (get b 0)))
     (abs (- (get a 1) (get b 1)))))

(defn+ possible-beacon? [point sensors-beacons]
  ; lol this function does the opposite of what it's called
  ; True if there can't possibly be a beacon here
  (any
      (map
        (fn+ [[sensor beacon]]
          (and (<= (manhattan point sensor)
                   (manhattan sensor beacon))
               (!= point beacon)
               (!= point sensor)))
        sensors-beacons)))

(defn count-tiles [sensors-beacons [y 10] [xminoff 1_000_000] [xmaxoff 1_000_000]]
  (let [beacon-xs (list (ap-map (first (second it))
                                sensors-beacons))
        xmin (- (min beacon-xs) xminoff) ; lol shitty subtract
        xmax (+ (max beacon-xs) xmaxoff)]
        ;xmin (- (min beacon-xs) xminoff) ; lol shitty subtract
        ;xmax (+ (max beacon-xs) xmaxoff)]
    (sum (ap-map (possible-beacon? #(it y) sensors-beacons)
                 (range xmin (+ 1 xmax))))))


(setv test
      [(Sensor at x=2, y=18: closest beacon is at x=-2, y=15)
       (Sensor at x=9, y=16: closest beacon is at x=10, y=16)
       (Sensor at x=13, y=2: closest beacon is at x=15, y=3)
       (Sensor at x=12, y=14: closest beacon is at x=10, y=16)
       (Sensor at x=10, y=20: closest beacon is at x=10, y=16)
       (Sensor at x=14, y=17: closest beacon is at x=10, y=16)
       (Sensor at x=8, y=7: closest beacon is at x=2, y=10)
       (Sensor at x=2, y=0: closest beacon is at x=2, y=10)
       (Sensor at x=0, y=11: closest beacon is at x=2, y=10)
       (Sensor at x=20, y=14: closest beacon is at x=25, y=17)
       (Sensor at x=17, y=20: closest beacon is at x=21, y=22)
       (Sensor at x=16, y=7: closest beacon is at x=15, y=3)
       (Sensor at x=14, y=3: closest beacon is at x=15, y=3)
       (Sensor at x=20, y=1: closest beacon is at x=15, y=3)])


(setv input [
             (Sensor at x=3844106, y=3888618: closest beacon is at x=3225436, y=4052707)
             (Sensor at x=1380352, y=1857923: closest beacon is at x=10411, y=2000000)
             (Sensor at x=272, y=1998931: closest beacon is at x=10411, y=2000000)
             (Sensor at x=2119959, y=184595: closest beacon is at x=2039500, y=-250317)
             (Sensor at x=1675775, y=2817868: closest beacon is at x=2307516, y=3313037)
             (Sensor at x=2628344, y=2174105: closest beacon is at x=3166783, y=2549046)
             (Sensor at x=2919046, y=3736158: closest beacon is at x=3145593, y=4120490)
             (Sensor at x=16, y=2009884: closest beacon is at x=10411, y=2000000)
             (Sensor at x=2504789, y=3988246: closest beacon is at x=3145593, y=4120490)
             (Sensor at x=2861842, y=2428768: closest beacon is at x=3166783, y=2549046)
             (Sensor at x=3361207, y=130612: closest beacon is at x=2039500, y=-250317)
             (Sensor at x=831856, y=591484: closest beacon is at x=-175938, y=1260620)
             (Sensor at x=3125600, y=1745424: closest beacon is at x=3166783, y=2549046)
             (Sensor at x=21581, y=3243480: closest beacon is at x=10411, y=2000000)
             (Sensor at x=2757890, y=3187285: closest beacon is at x=2307516, y=3313037)
             (Sensor at x=3849488, y=2414083: closest beacon is at x=3166783, y=2549046)
             (Sensor at x=3862221, y=757146: closest beacon is at x=4552923, y=1057347)
             (Sensor at x=3558604, y=2961030: closest beacon is at x=3166783, y=2549046)
             (Sensor at x=3995832, y=1706663: closest beacon is at x=4552923, y=1057347)
             (Sensor at x=1082213, y=3708082: closest beacon is at x=2307516, y=3313037)
             (Sensor at x=135817, y=1427041: closest beacon is at x=-175938, y=1260620)
             (Sensor at x=2467372, y=697908: closest beacon is at x=2039500, y=-250317)
             (Sensor at x=3448383, y=3674287: closest beacon is at x=3225436, y=4052707)])


(assert #dbg(= 25 (count-tiles test 9 1000 1000)))
(assert #dbg(= 26 (count-tiles test 10 1000 1000)))
(assert #dbg(= 27 (count-tiles test 11 1000 1000)))
(print "Passed 3 test cases")

(print
  "⭐ Day 15 part 1:"
  (count-tiles input 2_000_000))

; part 2

(defn+ line-coords [[startx starty] [endx endy]]
  (let [xdir (// (- endx startx)
                 (abs (- endx startx)))
        ydir (// (- endy starty)
                 (abs (- endx startx)))]
    (zip (range startx (+ xdir endx) xdir)
         (range starty (+ ydir endy) ydir))))


(defn+ perimeter [[[sx sy] beacon]]
  #dbg(list
        (let [dist (+ 1 (manhattan #(sx sy) beacon))
              left   #((- sx dist) sy)
              right  #((+ sx dist) sy)
              top    #(sx (- sy dist))
              bottom #(sx (+ sy dist))]
          (chain (line-coords left bottom)
                 (line-coords bottom right)
                 (line-coords right top)
                 (line-coords top left)))))


(defn find-beacon [sensors-beacons]
  (let [perimeters (chain #*(map perimeter sensors-beacons))
        ps-in-range (list (ap-filter (and (<= 0 (first it) 4_000_000)
                                          (<= 0 (second it) 4_000_000))
                                     perimeters))]
    (do #dbg (len ps-in-range))
    (- (set (ap-filter (not (possible-beacon? it sensors-beacons))
                       ps-in-range))
       (set (chain (map first sensors-beacons)
                   (map second sensors-beacons))))))

(print
  "⭐ Day 15 part 2:"
  (find-beacon input))
