; I'm getting annoyed at some of the decisions in hy's standard library,
; so I'm gonna start collecting a small utility library
(import typing [Iterable])

(defn first [lst]
  (get lst 0))

(defn second [lst]
  (get lst 1))

(defn rest [lst]
  (cut lst 1 (len lst)))

(defn last [lst]
  (get lst -1))

(defn readlines [fname]
  (tuple (map str.strip (.readlines (open fname)))))

(defn flatten-once [lst]
  (lfor sublist lst
        it sublist
        it))

(defn id [x] x)

(defn reclist [x]
  (cond ; this one is scuffed
    (isinstance x dict) x
    (isinstance x str) x
    (isinstance x tuple) x
    (isinstance x set) x
    (isinstance x Iterable) (list (map reclist x)))
    True x)

(defn all-coords [arr]
  ; all coords in a list or numpy array
  (lfor x (range (len (get heightmap 0)))
        y (range (len heightmap))
        #(x y)))

(defreader dbg
  (.slurp-space &reader)
  ( let [expr (.try_parse_one_form &reader)
        expr_str (hy.repr expr)]
    `(do
      (print f"{~(cut expr_str 1 None)}={~expr}")
      ~expr)))


(defreader time
  (.slurp-space &reader)
  ( let [expr (.try_parse_one_form &reader)
        expr_str (hy.repr expr)]
    `(do
       (import time [time])
       (let [begin (time)
            result ~expr
            duration (- (time) begin)]
        (print f"🕑 {~(cut expr_str 1 None)}={duration}")
        result))))
